const double PI = 2. * acos(0);
const double montecarlo = 1000.; // кол-во членов в интегральной сумме
const double l = 12.;// высота(м)
const double mu = 0.018;// кооф ослабления
const double length_t = 0.00002; // длитильность импульса (дельта)
const double delta_t = 0.4; // частота зондирования(c)
const double V = 1.; //скорость движения аппарата
const double c = 1500.; // скорость звука в воде(м/с)
const double alpha1 = (179 * PI) / 180.; // ширина диаграммы направленности
const double alpha2 = (80 * PI) / 180.;
const double beta = (20 * PI) / 180.; // ширина диаграммы направленности в плоскости k_2 = 0
const double sigma = mu*0.1;


const double Y1 = 40;
const double Y2 = 40;
const int M = int(Y2 / V / delta_t); // частота дискритизации(сколько раз принимаем)
const double h = 2*Y1/c; // шаг для сохранения пропорций изображения
const int N = int(h*(c/(2*V))/delta_t); // ширина

int coord_sigma_d[4][4] = {{36, 43, 17, 23}, 
                           {26, 33, 7, 13},
                           {10, 20, 15, 25},
                           {15, 25, 30, 40}};

int coord_sigma_d_circle[2][4] = {{35, 20, 30, 10}, {29, 15, 40, 19}};
int coord_index = 0;


template <typename T>
void free_2dim_array(T ***array, int n = N)
{
   for(int i = 0; i < n; ++i)
       delete[] (*array)[i];
   delete[] (*array);
   (*array) = nullptr;
}

template <typename T>
void create_2dim_array(T ***array, int n = N, int m = M)
{
   (*array) = new T*[n];
   for (int i = 0; i < n; i++) {
       (*array)[i] = new T[m];
       for (int j = 0; j < m; j++)
           (*array)[i][j] = 0;
   }
}

template <typename T>
T sqr(T var) { return var*var; }

template <typename T>
void print_array(T ** array, int n = N, int m = M)
{
   for (int i = 0; i < n; i++) {
       for (int j = 0; j < m; j++)
           cout << array[i][j] << " ";
       cout << endl;
   }
}

double l_i(double t_j, double t_i) { return c*(t_j - t_i)/2.; }

double ifnan(double x) {return (isnan(x) || isinf(x)) ? 0 : x; }

double mod_Vt(double k_2, double t_, double t)
{
	return ((t - t_) * (1 - sqr(V) / sqr(c)) / 2)*(c + (V*k_2) / (1 - (V * k_2) / c));
}
double mod_yx(double k_2, double t_, double t)
{
	return ((t - t_) * (1 - sqr(V) / sqr(c)) / 2)*(c - (V*k_2) / (1 - (V * k_2) / c));
}

double sigma_d(double y1, double y2)
{
	// two objects
  // if (sqrt(sqr(y1 - coord_sigma_d_circle[coord_index][0]) + sqr(y2 - coord_sigma_d_circle[coord_index][1])) < 7)
  //   return 0.6;
  // if (sqrt(sqr(y1 - coord_sigma_d_circle[coord_index][2]) + sqr(y2 - coord_sigma_d_circle[coord_index][3])) < 5)
  //   return 0.9;  
  // return 0.3;

  // one object
  if (sqrt(sqr(y1 - coord_sigma_d_circle[coord_index][0]) + sqr(y2 - coord_sigma_d_circle[coord_index][1])) < 5)
    return 0.9;  
  return 0.3;

    //if (y1 > 30 and y1 < 40 and y2 > 15 and y2 < 25)
    // if (y1 > coord_sigma_d[coord_index][0] and y1 < coord_sigma_d[coord_index][1] and y2 > coord_sigma_d[coord_index][2] and y2 < coord_sigma_d[coord_index][3])
    //     return 0.1;
    //return 0.5;
  //   double a=4.,s,b=5.;
  //       //if (sqrt(sqr(y_1-12)+sqr(y_2-6))<3) s=0.3;

  //       if (sqr((y_1-(12-b)*a)-(y_2-6*a))+3./10.*sqr((y_2-(12-b)*a)+(y_1-6*a))<sqr(3*a)) s=0.3;
  // else  if (sqrt(sqr(y_1-(17-b)*a)+sqr(y_2-3*a))<sqr(a/2.)) s=0.2;
  // else  if (sqrt(sqr(y_1-(8-b)*a)+sqr(y_2-9*a))<sqr(a/2.)) s=0.25;
  // else s=0.1;
  // return s;
}

double sigma_d(double t, double t_, double k_2)
{
	double y2 = V*t - k_2*mod_Vt(k_2, t_, t);
	double y1 = ifnan(sqrt(sqr((c*(t - t_) / 2)*(1 - sqr(V) / sqr(c)) + (V / c)*(V*t - y2)) - sqr(V*t - y2) - sqr(l)));
	return sigma_d(y1, y2);
}

bool print_bin(double **sigma_d, double sigma_min = 0, double sigma_max = 0.3, int n = N, int m = M)
{
	FILE *f_out = fopen("sigma_d.bin", "wb");
	if (!f_out)
		return 0;

    // sigma_min = 1000;
    // sigma_max = -sigma_min;
    // for (int i = 0; i < n*m; ++i)
    // {
    //     if (sigma_d[i][0] < sigma_min)
    //         sigma_min = sigma_d[i][0];
    //     if (sigma_d[i][0] > sigma_max)
    //         sigma_max = sigma_d[i][0];
    // }
    // cout << sigma_min << " " << sigma_max << endl;

	unsigned char value = 0;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (sigma_d[i*n+j][0] <= sigma_min)
				value = 0;
			else if (sigma_d[i*n+j][0] >= sigma_max)
				value = 255;
			    else 
				{
					value = char((sigma_d[i*n+j][0] - sigma_min) / (sigma_max - sigma_min)*255.);
                    //cout << value << " ";
				}
			fwrite(&value, sizeof(char), 1, f_out);
		}
	}
	fclose(f_out);
	return true;
}

double har(double x, double a, double b) { return ((x >= a) && (x <= b)); }

double y1(double k_2, double t_, double t)
{
	double p = mod_Vt(k_2, t_, t);
	return sqrt(sqr(p) - sqr(k_2*p) - sqr(l));
}

double yakobian_g(double k_2, double t_, double t)
{
	double Vt = mod_Vt(k_2, t_, t);
	double yx = mod_yx(k_2, t_, t);
	return (y1(k_2, t_, t) / (c*Vt))*((1 / Vt) - (k_2*V*(t - t_) / (Vt*yx)) + (1 / yx));
}

double mes() {
    double sum=0;
    double limit=cos(PI/2. - beta);
    double h = 2.*limit/montecarlo;
    for (double x=-limit;x<limit;x+=h)
        sum+=exp(-sqr(x/(beta)));
    return sum;
}

double I_g(double t_j, double t_i)
{
	double _exp, delta;
	double k_3;
	double sum;
	double k_2 = 0., t_ = 0.;
    double limit = cos(PI/2. - beta);

	sum = 0.;

	for (int j = 0; j < montecarlo; j++)
	{
		double Vt;
		double yx;
        double y_1;

		//t_ = ((rand() / (double)RAND_MAX)*(-length_t) + length_t / 2. + t_i);
        t_ = t_i;
		//delta = sin(beta) * ifnan(sqrt(1 - sqr(k_3)));

		k_2 = ((rand() / (double)RAND_MAX)*(-2 * limit) + limit);

		Vt = mod_Vt(k_2, t_, t_j);
		yx = mod_yx(k_2, t_, t_j);
        y_1 = Vt * sqrt(1-sqr(k_2)-sqr(l/Vt));

		// _exp = har(-k_3, cos(alpha1), cos(alpha2)) * exp(-mu * (Vt + yx));

		// _exp *= l / (pow(Vt, 3) * sqr(yx) *  abs(yakobian_g(k_2, t_, t_j)));

		// _exp *= l*exp(-sqr(k_2/beta)) / yx;

		// sum += _exp * sigma_d(t_j, t_, k_2) / PI;
        sum+= ifnan(exp(-sqr(k_2/(beta)))*sigma_d(t_j, t_, k_2)/(sqr(yx)*Vt*y_1*(c-V*k_2)));
    
    }///Vt*y1*(c-V*k_2)))*c*sqr(l)/PI*exp(-mu*c*(t_j-t_i))/(t_j-t_i);
                
return sum * c*sqr(l)/PI*exp(-mu*c*(t_j-t_i))/(t_j-t_i)/mes();
}

void create_signal_b(matrix *b){
	int k = 0;
	double t_i, t_j;
   	for (int i = 0; i < M; i++) {
       	t_i = i*delta_t;
       	for (int j = 0; j < N; j++) { 
        	t_j = t_i + j*h/N + 2*l/c + length_t;
        	b->a[i*N+j][0] = I_g(t_j, t_i);
        	//cout << (*b).a[k-1][0] << " " ;        	
       }
   }
   //cout << "k " << k << endl;
}


void create_A(matrix *a){

    int mk = montecarlo;
    mk = 100;
	double t_i, t_j;
	double limit = cos(PI/2. - beta);
	double h_k2 = 2 * limit / (mk);
	double Vt, Vti;
	double y1, y2;
	int p, q;
	double buff;

	int ma = 0;
   	for (int i = 0; i < M; i++) {
       	t_i = i*delta_t;
       	for (int j = 0; j < N; j++) { 
        	t_j = t_i + j*h/N + 2*l/c + length_t;
        	for (double k_2=-limit;k_2<=limit;k_2+=h_k2)
        	{
        		Vt = sqr(c) * (t_j - t_i) / (2 * (c - V * k_2) ) * (1. - sqr(V/c));
        		Vti = Vt * (c - V * k_2) / (c + V * k_2);
        		y1 = Vt * sqrt(1 - sqr(k_2) - sqr(l/Vt));
        		y2 = V*t_j - k_2*Vt;
        		buff = (2.*(c-V*k_2)/sqr(c)*sqrt((sqr(y1)+sqr(l))/(1-sqr(k_2))) - 2.*l/c) / (h/N); //!!!
        		p = int(buff);     //stolbets
        		ma = max(p, ma);
            	buff = y2/(V*delta_t);
            	q = int(buff); 
            	double mer=mes();

            	if ((q >= 0) and (q < N) and (p >= 0) and (p < M)) {
                	a->a[i* N +j][q*M +p] += ifnan(exp(-sqr(k_2/beta))/(sqr(Vti)*Vt*y1*(c-V*k_2)))/mer*c*sqr(l)/PI*exp(-mu*c*(t_j-t_i))/(t_j-t_i);
                }
        	}
       }
   }
   //cout << "max p = " << ma << a->m <<" " << a->n <<  endl;
}

double test(double t, double t_j)
{
    double y_1 = sqrt(sqr(l_i(t, t_j)) - sqr(l));
    return (2. * PI * pow(l_i(t, t_j), 4) * y_1 * exp(2.*mu*l_i(t, t_j))) / (c*sqr(l));
}

matrix direct_problem(const matrix &b)
{
    double y_2, y_1, tmp;
    matrix temp =  matrix(N, M);
    double t_j, t;

    for (int j = 0; j < b.m; j++)
    {
        tmp = 0;
        for (int i = 0; i < b.n; i++)
        {

            t_j = delta_t * i;
            t  = t_j + j*h/N + 2*l/c + length_t;
            y_1 = sqrt(sqr(l_i(t, t_j)) - sqr(l));
            y_2 = V*t_j;
            tmp = (2. * PI * pow(l_i(t, t_j), 4) * y_1 * exp(2.*mu*l_i(t, t_j))) / (c*sqr(l));
            //tmp *= I_g(t, t_j);
            //tmp *= b.a[i][j];
            
            //tmp -= sigma * sqr(l_i(t, t_j))*y_1 *A(t, t_j) / (4 * sqr(l));

            temp.a[i][j] = test(t, t_j)*I_g(t, t_j);  
        }
    }

    return temp;
}

void bottom(matrix &b)
{
    double y_2, y_1;
    double t_j, t;
    for (int j = 0; j < b.m; j++)
    {
        for (int i = 0; i < b.n; i++)
        {
            t_j = delta_t * i;
            t  = t_j + j*h/N + 2*l/c + length_t;
            y_1 = sqrt(sqr(l_i(t, t_j)) - sqr(l));
            y_2 = V*t_j;
            b.a[i][j] = sigma_d(y_1, y_2);  
        }
    }
}

matrix rotate_matrix(const matrix &sigma)
{
  matrix temp = matrix(sigma.n, sigma.m);

  int n = sigma.n-1;
  int m = sigma.m-1;
  for (int i = 0; i < sigma.n; ++i)
  {
    for (int j = 0; j < sigma.m; ++j)
    {
      temp.a[i][j] = sigma.a[n-j][m-i];
    }
  }
  return temp;
}

double norm_2(const matrix &b)
{
   double sum=0.;
   for (int i = 0; i < b.n; i++)
      for (int j = 0; j < b.m; j++)
         sum+=sqr(b.a[i][j]);
   sum /= double(b.n*b.m);
   return sqrt(sum);
}
double norm_2_2(const matrix &a, const matrix &b)
{
   double sum=0.;
   for (int i = 0; i < a.n; i++)
      for (int j = 0; j < a.m; j++)
         sum+=sqr(a.a[i][j] - b.a[i][j]);
   sum /= double(a.n*a.m);
   return sqrt(sum);
}

double norm_max_relative(const matrix &a, const matrix &b)
{
   double max=0.;
   double tmp = 0.;
   for (int i = 0; i < a.n; i++)
      for (int j = 0; j < a.m; j++){         
         tmp = abs((a.a[i][j] - b.a[i][j]) / a.a[i][j]);
         if (tmp > max) max = tmp;
          }
   return (max);
}

void rotate_sigma()
{ 
   coord_index++;
}
