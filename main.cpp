#include <cmath>
#include <iostream>
#include <string>
#include <unistd.h>
#include <cfloat> //for DBL_MAX


using namespace std;


#include "../matrix/matrix.cpp"
#include "functions.cpp"




int main(int argc, const char * argv[]) {

    matrix sigma = matrix(N, M);
    matrix sigma_one = matrix(N, M);
    matrix sigma_two = matrix(N, M);
    matrix direct = matrix(N, M);
    matrix tmp = matrix(N, M);
    int rotate_count = 4;

    coord_index = 0;
    bottom(direct);
    sigma_one = direct_problem(sigma_one);
    rotate_sigma();
    sigma_two = direct_problem(sigma_two);
    sigma_two = rotate_matrix(sigma_two);

    for (int i = 0; i < sigma.n; ++i)
    {
      for (int j = 0; j < sigma.m; ++j)
      {
        //sigma.a[i][j] = (sigma.a[i][j]*2 + sigma_top.a[i][j]*2)/4;
        // sigma.a[i][j] = min(sigma.a[i][j], sigma.a[i][j]);
        // sigma.a[i][j] = min(sigma_top.a[i][j], sigma_top.a[i][j]);
        sigma.a[i][j] = min(sigma_one.a[i][j], sigma_two.a[i][j]);
      }
    }

    // for (int i = 0; i < sigma.n; ++i)
    // {
    //   sigma.a[i][63] = 0;
    //   sigma.a[50][i] = 0;
    //   sigma.a[i][52] = 0;
    //   sigma.a[26][i] = 0;
    // }

    sigma.print_bin();
    //cout << norm_2(direct) << "\t" << norm_2(sigma) << endl;
    cout << "sigma_one" << endl;
    cout << norm_2_2(direct, sigma_one) / norm_2(direct) << endl;
    cout << norm_max_relative(direct, sigma_one) << endl;

    cout << endl << "sigma_two" << endl;
    cout << norm_2_2(direct, sigma_two) / norm_2(direct) << endl;
    cout << norm_max_relative(direct, sigma_two) << endl;
    
    cout << endl << "sigma" << endl;
    cout << norm_2_2(direct, sigma) / norm_2(direct) << endl;
    cout << norm_max_relative(direct, sigma) << endl;

    //A.print_bin();

    // FILE *f_out = fopen("singal.bin", "wb");
    // for (int i = 0; i < N; i++)
    // {
    //   for (int j = 0; j < M; ++j)
    //   {
    //     if (i==50)
    //       fwrite(&b.a[i*N+j][0], sizeof(double), 1, f_out);
    //   }
    // }
    // fclose(f_out);
    //узкая диаграмма направл
  // int k = 0;
  // double t_i, t_j;
  //   for (int i = 0; i < M; i++) {
  //       t_i = i*delta_t;
  //       for (int j = 0; j < N; j++) { 
  //         t_j = t_i + j*h/N + 2*l/c + length_t;
  //         b.a[i*N+j][0] = test(t_j, t_i)*I_g(t_j, t_i);
  //         cout << test(t_j, t_i)*I_g(t_j, t_i) << " " ;         
  //      }
  //  }
  



   
    // cout << Y1 << " " << Y2 << endl;
    // cout << N << " " << M << endl;
    // print_bin(mas);
    system(("python3 convert.py " + to_string(N) + " " + to_string(M)).c_str());
    // //print_array(mas);
    return 0;
}